# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
  url(r'^location/(?P<location_pk>\d+)/$', 'harjoitustyo.views.location_view', name='location_view'),
  url(r'^location/$', 'harjoitustyo.views.location_list', name='location_list'),
  url(r'^view/json/$', 'harjoitustyo.views.view_json', name='view_json'),
  url(r'^view/(?P<view_pk>\d+)/$', 'harjoitustyo.views.view_edit', name='view_edit'),
  url(r'^view/$', 'harjoitustyo.views.view_list', name='view_list'),
  url(r'^user/(?P<user_pk>\d+)/edit/$', 'harjoitustyo.views.user_edit', name='user_edit'),
  url(r'^user/$', 'harjoitustyo.views.user_list', name='user_list'),
  url(r'^object/(?P<object_pk>\d+)/set/(?P<value>\d+)/(?P<source>\d+)/$', 'harjoitustyo.views.object_set_value', name='object_set_value'),
  url(r'^', 'harjoitustyo.views.view', name='view'),
)
