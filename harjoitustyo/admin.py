from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from harjoitustyo.models import *

admin.site.register(Location)
admin.site.register(Room)
admin.site.register(Object)
admin.site.register(ViewCategory)
admin.site.register(ViewCategoryObject)

class ViewCategoryInline(admin.TabularInline):
  model = ViewCategory
  extra = 0

class ViewAdmin(admin.ModelAdmin):
  inlines = [
    ViewCategoryInline,
  ]

admin.site.register(View, ViewAdmin)

class PersonInline(admin.StackedInline):
  model = Person
  can_delete = False

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (PersonInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
