$(function() {
  /* Add new row */
  $('#add-new-row').on('click', function(e) {
    e.preventDefault();
    $('#empty-row-form .form-row')
      .clone()
      .appendTo('#rows');
  });

  /* Delete single rows */
  $('#rows').on('click', '.delete-row', function(e) {
    console.log('del');
    e.preventDefault();
    $(this)
      .closest('.form-row')
      .remove();
  });

  /* Fix indexes and management form on form submit */
  $('form').on('submit', function() {
    // Fix indexes
    $('#rows .row').each(function(i) {
      // Labels
      $(this).find('label').each(function() {
        $(this).attr('for', $(this).attr('for').replace(/CATEGORIES-.*-/, "CATEGORIES-" + i + "-"));
      });

      // Inputs & Selects
      $(this).find('input, select').each(function() {
        $(this).attr('id', $(this).attr('id').replace(/CATEGORIES-.*-/, "CATEGORIES-" + i + "-"));
        $(this).attr('name', $(this).attr('name').replace(/CATEGORIES-.*-/, "CATEGORIES-" + i + "-"));
      });
    });

    // Fix management form
    $('#id_viewform_CATEGORIES-TOTAL_FORMS').val($('#expenses .expenseline').length);

    // Let the form to be submitted
    return true;
  });
});

var refreshId = setInterval(function() {
  $.getJSON( "/view/json/", function( data ) {
    $.each( data, function( key, val ) {
      $("#value-" + key).html(val);
    });
  });
}, 5000);
