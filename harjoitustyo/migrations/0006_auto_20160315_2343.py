# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-15 23:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('harjoitustyo', '0005_auto_20160315_2333'),
    ]

    operations = [
        migrations.RenameField(
            model_name='object',
            old_name='type',
            new_name='object_type',
        ),
        migrations.RenameField(
            model_name='viewcategory',
            old_name='objects',
            new_name='object_list',
        ),
    ]
