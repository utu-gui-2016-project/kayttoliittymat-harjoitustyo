# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy
from django.db.models import Max
from django.core.urlresolvers import reverse

class Location(models.Model):
  """
  Represents a single location containing rooms.
  """
  name = models.CharField('Name', max_length=255)

  def __unicode__(self):
    return self.name

  def get_absolute_url(self):
    return reverse('location_view', kwargs={'location_pk': self.id})

class Room(models.Model):
  """
  Represents a room in a single location containing objects.
  """
  name = models.CharField('Name', max_length=255)
  location = models.ForeignKey(Location)

  def __unicode__(self):
    return self.name + " (" + unicode(self.location) + ")"

  def get_objects(self):
    return Object.objects.filter(room=self)

OBJECT_TYPE_CHOICES = (
  (u'PERC', u'Percentage'),
  (u'BOOL', u'Boolean'),
  (u'TEMPC', u'Temperature in Celsius'),
)

class Object(models.Model):
  """
  Represents an object in a single room of a single location. Object is a
  single sensor or gadget.
  """
  name = models.CharField('Name', max_length=255)
  room = models.ForeignKey(Room)
  object_type = models.CharField('Type', max_length=5, choices=OBJECT_TYPE_CHOICES)
  value = models.CharField('Value', max_length=255)

  def __unicode__(self):
    return self.name + " (" + unicode(self.room) + ")"

class View(models.Model):
  """
  Represents a single view that contains categories with objects.
  """
  name = models.CharField('Name', max_length=255)

  def __unicode__(self):
    return self.name

class ViewCategory(models.Model):
  """
  Represents a category inside a view. Contains objects.
  """
  name = models.CharField('Name', max_length=255)
  view = models.ForeignKey(View)
  object_list = models.ManyToManyField(Object, through='ViewCategoryObject')

  def __unicode__(self):
    return self.name + " (" + unicode(self.view) + ")"

  def get_objects(self):
    objects = []
    # Generate an iterable list of objects in the category. The main thing to remember
    # here is that name must be overrideable, so ViewCategoryObject.name should be
    # taken into account.
    for view_object in ViewCategoryObject.objects.filter(view_category=self):
      object = {
        'object': view_object.object,
        'id': view_object.object.id,
        'name': view_object.object.name,
        'value': view_object.object.value,
        'room': view_object.object.room,
        'object_type': view_object.object.object_type,
      }
      # Override if defined
      if view_object.name:
        object['name'] = view_object.name
      # And finally append to the list
      objects.append(object)
    return objects

class ViewCategoryObject(models.Model):
  """
  Represents an object in a category. Allows renaming the object in a
  category.
  """
  object = models.ForeignKey(Object)
  name = models.CharField('Object name', max_length=255, blank=True)
  view_category = models.ForeignKey(ViewCategory)

  def __unicode__(self):
    return self.name + " (" + unicode(self.view_category) + ")"

class Person(models.Model):
  """
  Represents the additional data required for a single user.
  """
  user = models.OneToOneField(User)

  view = models.ForeignKey(View)

  def __unicode__(self):
    return unicode(self.user)

def createPerson(sender, user, request, **kwargs):
  Person.objects.get_or_create(user=user)

