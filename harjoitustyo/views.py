# -*- coding: utf-8 -*-

from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, render, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, JsonResponse
from collections import deque
from copy import copy

from harjoitustyo.models import Location, Room, Object, View, ViewCategory, Person
from harjoitustyo.forms import UserForm, PersonForm, ViewForm
from django.contrib.auth.models import User

@login_required
def view(request):
  """
  Fetch and render the view defined for the logged-in user, if defined.
  """
  view = get_object_or_404(View, person=request.user.person)

  categories = ViewCategory.objects.filter(view=view)

  return render(request, 'view.html', {
    'name': view,
    'categories': categories,
    'source': 1,
  })

@login_required
def view_json(request):
  """
  Return the status of an object as JSON for AJAX update checks.
  """
  view = get_object_or_404(View, person=request.user.person)
  categories = ViewCategory.objects.filter(view=view)
  objects = {}
  for category in categories:
    for object in category.get_objects():
      objects[object['id']] = object['value']

  return JsonResponse(objects)

@staff_member_required
def location_list(request):
  return render(request, 'location_list.html', {
    'locations': Location.objects.all(),
  })

@staff_member_required
def location_view(request, location_pk):
  """
  Render a location page including it's rooms and objects.
  """
  location = get_object_or_404(Location, id=location_pk)
  rooms = Room.objects.filter(location=location)

  return render(request, 'view.html', {
    'view': location,
    'categories': rooms,
    'source': 0,
  })

@staff_member_required
def view_list(request):
  """
  List defined views.
  """
  return render(request, 'view_list.html', {
    'views': View.objects.all(),
  })

@staff_member_required
def view_edit(request, view_pk):
  """
  Edit view.
  """
  view = get_object_or_404(View, id=view_pk)

  form = ViewForm((request.POST or None),
    instance=view,
    prefix='viewform')

  # If this is a POST request we need to process the form data
  if form.is_valid():
    form.save()
    # Redirect to a new URL:
    messages.success(request, 'View details updated.')
    return HttpResponseRedirect(reverse('view_edit', args=[view.id]))

  testi = dir(form)

  return render(request, 'view_edit.html', {
    'view': view,
    'form': form,
  })

@login_required
def object_set_value(request, object_pk, value, source):
  """
  Set object value and redirect the user back to the view or location.
  """
  object = get_object_or_404(Object, id=object_pk)
  object.value = value
  object.save()

  if source == '0':
    target = object.room.location
  else:
    target = 'view'

  return redirect(target)

@staff_member_required
def user_list(request):
  """
  List all users.
  """
  users = User.objects.all()

  return render(request, 'user_list.html', {
    'users': users,
  })

@staff_member_required
def user_edit(request, user_pk):
  """
  Render a form for editing user data and save the submitted changes.
  """
  user = get_object_or_404(User, id=user_pk)
  # Provide the original user object for rendering to prevent unsaved changes
  # from showing up on the rendered form header etc.
  orig_user = copy(user)
  person = get_object_or_404(Person, user=user)

  # If this is a POST request we need to process the form data
  if request.method == 'POST':
    uform = UserForm(request.POST, instance=user)
    pform = PersonForm(request.POST, instance=person)
    if uform.is_valid() and pform.is_valid():
      if uform.cleaned_data['new_password']:
        user.set_password(uform.cleaned_data['new_password'])

      uform.save()
      pform.save()
      # Redirect to a new URL:
      messages.success(request, 'Profile details updated.')
      return HttpResponseRedirect(reverse('user_edit', args=[user.id]))

  # If a GET (or any other method) we'll create a blank form
  else:
    uform = UserForm(instance=user)
    pform = PersonForm(instance=person)

  return render(request, 'user_edit.html', {
    'user': orig_user,
    'uform': uform,
    'pform': pform,
  })
