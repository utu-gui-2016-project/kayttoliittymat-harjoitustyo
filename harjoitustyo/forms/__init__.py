from django import forms
from django.contrib.auth.models import User
from harjoitustyo.models import Person, View, ViewCategory
from django.forms.models import inlineformset_factory
from harjoitustyo.forms.inline_snippet import *

class BaseForm(ModelForm):
  def __init__(self, *args, **kwargs):
    super(BaseForm, self).__init__(*args, **kwargs)
    for field_name, field in self.fields.items():
#      if field.widget.__class__.__name__ != 'CheckboxInput':
      field.widget.attrs['class'] = 'form-control'

class UserForm(BaseForm):
  new_password = forms.CharField(widget=forms.PasswordInput(), required=False)
  new_password_confirmation = forms.CharField(widget=forms.PasswordInput(), required=False)

  class Meta:
    model = User
    exclude = ['user_permissions', 'groups', 'is_staff', 'date_joined', 'last_login', 'password',]

  def is_valid(self):
    # run the parent validation first
    valid = super(UserForm, self).is_valid()

    # we're done now if not valid
    if not valid:
      return valid

    if self.cleaned_data['new_password'] and self.cleaned_data['new_password_confirmation']:
      if self.cleaned_data['new_password'] != self.cleaned_data['new_password_confirmation']:
        self._errors['new_password_confirmation'] = ['The passwords don\'t match',]
        return False

    return True

class PersonForm(BaseForm):
  class Meta:
    model = Person
    exclude = ['user',]

class ViewCategoryForm(BaseForm):
  class Meta:
    model = ViewCategory
    exclude = []

ViewCategoryFormset = inlineformset_factory(View, ViewCategory, form=ViewCategoryForm, extra=1, can_delete=False)

class ViewForm(BaseForm):
  class Meta:
    model = View
    exclude = []

  class Forms:
    inlines = {
      'categories': ViewCategoryFormset,
    }
